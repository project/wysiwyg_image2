CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration


INTRODUCTION
------------

This module provides "Enhanced Image" plugin for CKEditor installed
with Wysiwyg.

For a full description of the project visit the project page:
https://www.drupal.org/project/wysiwyg_image2

To submit bug reports and feature suggestions, or to track changes:
https://www.drupal.org/project/issues/wysiwyg_image2


REQUIREMENTS
------------

This module requires the following module:

 * Wysiwyg (https://www.drupal.org/project/wysiwyg)


INSTALLATION
------------

 * Install as usual. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

 * Download "Enhanced Image":
   https://ckeditor.com/cke4/addon/image2,

   unzip it and place "image2" directory into
   "sites/all/libraries/ckeditor/plugins" directory. As a result there will
   be "plugin.js" file in "sites/all/libraries/ckeditor/plugins/image2"
   directory.


CONFIGURATION
-------------

 * Go to "Wysiwyg profiles" page: /admin/config/content/wysiwyg.

 * Edit a Wysiwyg profile:

   - disable "Image" button if it is enabled;

   - enable "Enhanced Image" button.
